/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#ifdef __cplusplus
extern "C" {
#endif    
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include "lcd.h"

static inline void LCD_WR_DATA_16(uint16_t data){
//LCD_DATO();
WR_ON();
GPIOF->ODR = data;
WR_OFF();
//CS_OFF();
}

static void DRV_PMP0_Write(uint8_t data){
	CS_ON();
	WR_ON();
	TFT_D0_GPIO_Port->ODR = data;
	WR_OFF();
	CS_OFF();
}

static void LCD_WR_REG(uint8_t data){
LCD_COMANDO();
DRV_PMP0_Write(data); }
static void LCD_WR_DATA(uint8_t data){
LCD_DATO();
DRV_PMP0_Write(data); }

static void LCD_direction(void){
    /*Direccion de columna*/
    LCD_WR_REG(0x2A);
    LCD_WR_DATA(0x00);
    LCD_WR_DATA(0x00);
    LCD_WR_DATA(0x01);
    LCD_WR_DATA(0xDF);
    /*Direccion de fila*/
    LCD_WR_REG(0x2B);
    LCD_WR_DATA(0x00);
    LCD_WR_DATA(0x00);
    LCD_WR_DATA(0x01);
    LCD_WR_DATA(0x3F); }

//extern void LCD_Clear(uint16_t c){
//  static size_t i;
//  LCD_direction();
//    LCD_WR_REG(0x2C); //Esperando datos
//	for(i=0;i!=(LCD_W*LCD_H)-1;i++){
//    LCD_WR_DATA(((uint8_t*)&c)[1]);
//    LCD_WR_DATA(((uint8_t*)&c)[0]);
//     }
//}

extern void LCD_Clear(uint16_t c){
  static size_t i;
  LCD_direction();
    LCD_WR_REG(0x2C); //Esperando datos
	for(i=0;i!=(LCD_W*LCD_H)-1;i++){
    LCD_WR_DATA_16(c);
     }
}


extern inline void LCD_Paint_16(uint16_t *datos, uint32_t size){
uint32_t t;
//LCD_DATO();
//CS_ON();
for(t=0;t!=size;t++){
LCD_WR_DATA_16(((uint16_t*)datos)[t]); }
}


extern void LCD_Paint(uint8_t *datos,uint32_t size){
static uint32_t t;
for(t=0;t!=size;t++){
LCD_WR_DATA(*(datos+t)); }
}

extern void cuadro(int16_t x_p, int16_t y_p, uint16_t c){
  /*Revisando que no se pasen del plano cartesiano :v*/
    
    //CREAR LUEGO
static uint16_t x,y,x0,x1,y0,y1;
x0 = x_p - SQ_SZ;
x1 = x_p + SQ_SZ;
y0 = y_p - SQ_SZ;
y1 = y_p + SQ_SZ;
   /*X = 480*/ 
   LCD_WR_REG(0x2A);
   LCD_WR_DATA(((uint8_t*)&x0)[1]);
   LCD_WR_DATA(((uint8_t*)&x0)[0]);
   LCD_WR_DATA(((uint8_t*)&x1)[1]);
   LCD_WR_DATA(((uint8_t*)&x1)[0]);
   /*Y = 320*/
   LCD_WR_REG(0x2B);
   LCD_WR_DATA(((uint8_t*)&y0)[1]);
   LCD_WR_DATA(((uint8_t*)&y0)[0]);
   LCD_WR_DATA(((uint8_t*)&y1)[1]);
   LCD_WR_DATA(((uint8_t*)&y1)[0]);

 x = x1 - x0;
 y = y1 - y0;
 ++x;
 static size_t i;
  LCD_WR_REG(0x2C); //Esperando datos
   for(i=0;i!=(x*y);i++){
    LCD_WR_DATA(((uint8_t*)&c)[1]);
     LCD_WR_DATA(((uint8_t*)&c)[0]); }   
}

extern void LCD_Init(void){
HAL_Delay(100);
LCD_WR_REG(0x36);
LCD_WR_DATA(0x68);
LCD_WR_REG(0x3A);
LCD_WR_DATA(0x55);
LCD_WR_REG(0xB6);
LCD_WR_DATA(0x20);
LCD_WR_DATA(0x02);
LCD_WR_REG(0xB5);
LCD_WR_DATA(0x02);
LCD_WR_DATA(0x03);
LCD_WR_DATA(0x00);
LCD_WR_DATA(0x04);
LCD_WR_REG(0xB1);
LCD_WR_DATA(0x80);
LCD_WR_DATA(0x10);
LCD_WR_REG(0xB4);
LCD_WR_DATA(0x00);
LCD_WR_REG(0xB7);
LCD_WR_DATA(0xC6);
LCD_WR_REG(0xC5);
LCD_WR_DATA(0x24);
LCD_WR_REG(0xE4);
LCD_WR_DATA(0x31);
LCD_WR_REG(0xE8);
LCD_WR_DATA(0x40);
LCD_WR_DATA(0x8A);
LCD_WR_DATA(0x00);
LCD_WR_DATA(0x00);
LCD_WR_DATA(0x29);
LCD_WR_DATA(0x19);
LCD_WR_DATA(0xA5);
LCD_WR_DATA(0x33);
LCD_WR_REG(0xC2);
LCD_WR_REG(0xA7);
LCD_WR_REG(0xE0);
LCD_WR_DATA(0xF0);
LCD_WR_DATA(0x09);
LCD_WR_DATA(0x13);
LCD_WR_DATA(0x12);
LCD_WR_DATA(0x12);
LCD_WR_DATA(0x2B);
LCD_WR_DATA(0x3C);
LCD_WR_DATA(0x44);
LCD_WR_DATA(0x4B);
LCD_WR_DATA(0x1B);
LCD_WR_DATA(0x18);
LCD_WR_DATA(0x17);
LCD_WR_DATA(0x1D);
LCD_WR_DATA(0x21);
LCD_WR_REG(0XE1);
LCD_WR_DATA(0xF0);
LCD_WR_DATA(0x09);
LCD_WR_DATA(0x13);
LCD_WR_DATA(0x0C);
LCD_WR_DATA(0x0D);
LCD_WR_DATA(0x27);
LCD_WR_DATA(0x3B);
LCD_WR_DATA(0x44);
LCD_WR_DATA(0x4D);
LCD_WR_DATA(0x0B);
LCD_WR_DATA(0x17);
LCD_WR_DATA(0x17);
LCD_WR_DATA(0x1D);
LCD_WR_DATA(0x21);
LCD_WR_REG(0X36);
LCD_WR_DATA(0xEC);
LCD_WR_REG(0xF0);
LCD_WR_DATA(0xC3);
LCD_WR_REG(0xF0);
LCD_WR_DATA(0x69);
LCD_WR_REG(0X13);
LCD_WR_REG(0X11);
LCD_WR_REG(0X29);
LCD_Clear(BG_COLOR);
LCD_DATO();
CS_ON();
}


/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif
/* *****************************************************************************
 End of File
 */
