/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#ifndef _LCD_H    /* Guard against multiple inclusion */
#define _LCD_H
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include "main.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Definiciones                                                      */
/* ************************************************************************** */
/* ************************************************************************** */

#define GIF						(1)
#define FRAME_NUM				(90) //ice 90, firepit 5, naruto 24, egg 10

#define LCD_W                   (320)
#define LCD_H                   (480)

#define IMA_BUFF_SIZE			(LCD_W*LCD_H)

#define BG_COLOR                (WHITE)
#define CMD_RDX                 (0xD0)
#define CMD_RDY                 (0x90)
#define SQ_SZ                      (4)
/*Control*/
#define LCD_DATO()  	   (TFT_RS_GPIO_Port->BSRR = (TFT_RS_Pin))
#define LCD_COMANDO()      (TFT_RS_GPIO_Port->BSRR = (uint32_t)TFT_RS_Pin << 16U)
//Chip Select active on LOW
#define CS_ON()            (TFT_CS_GPIO_Port->BSRR = (uint32_t)TFT_CS_Pin << 16U)
#define CS_OFF()           (TFT_CS_GPIO_Port->BSRR = (TFT_CS_Pin))
//Chip Reset signal active LOW
#define RESET_ON           (TFT_RST_GPIO_Port->BSRR = (uint32_t)TFT_RST_Pin << 16U)
#define RESET_OFF          (TFT_RST_GPIO_Port->BSRR = (TFT_RST_Pin))
//Write Data
#define WR_ON()			   (TFT_WR_GPIO_Port->BSRR = (uint32_t)TFT_WR_Pin << 16U)
#define WR_OFF()		   (TFT_WR_GPIO_Port->BSRR = (TFT_WR_Pin))
//Read Data
#define RD_ON()			   (TFT_RD_GPIO_Port->BSRR = (uint32_t)TFT_RD_Pin << 16U)
#define RD_OFF()		   (TFT_RD_GPIO_Port->BSRR = (TFT_RD_Pin))
/*Colores*/
#define WHITE       (0xFFFF)
#define BLACK      	(0x0000)
#define BLUE       	(0x001F)
#define BRED        (0xF81F)
#define GRED        (0xFFE0)
#define GBLUE       (0x07FF)
#define RED         (0xF800)
#define MAGENTA     (0xF81F)
#define GREEN       (0x07E0)
#define CYAN        (0x7FFF)
#define YELLOW      (0xFFE0)
#define BROWN 		(0xBC40)
#define BRRED 		(0xFC07)
#define GRAY  		(0x8430)
#define DARKBLUE    (0x01CF)
#define LIGHTBLUE   (0x7D7C)
#define GRAYBLUE    (0x5458)
#define LIGHTGREEN  (0x841F)
#define LIGHTGRAY   (0xEF5B)
#define LGRAY 		(0xC618)
#define LGRAYBLUE   (0xA651)
#define LBBLUE      (0x2B12)
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Estructuras                                                       */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Estructuras                                                       */
/* ************************************************************************** */
/* ************************************************************************** */
void LCD_Init(void);
void LCD_Paint(uint8_t *datos,uint32_t size);
void cuadro(int16_t x_p, int16_t y_p, uint16_t c);
void LCD_Clear(uint16_t c);
void LCD_Paint_16(uint16_t *datos, uint32_t size);
/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif
#endif
/* *****************************************************************************
 End of File
 */
